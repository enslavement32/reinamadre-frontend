import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroup, FormBuilder, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BookService } from '../book.service';

@Component({
  selector: 'app-add-book',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css'],
})
export class AddBookComponent {
  FormData!: FormGroup;
  isloading!: boolean;

  constructor(
    private builder: FormBuilder,
    private book: BookService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.FormData = this.builder.group({
      fecha: new FormControl(''),
      paciente: new FormControl(''),
      tipoCita: new FormControl(''),
      nombreMedico: new FormControl(''),
      numeroCita: new FormControl(''),
      estado: new FormControl(''),
    });
  }

  reset() {
    this.FormData = this.builder.group({
      fecha: new FormControl(''),
      paciente: new FormControl(''),
      tipoCita: new FormControl(''),
      nombreMedico: new FormControl(''),
      numeroCita: new FormControl(''),
      estado: new FormControl(''),
    });
  }

  onSubmit(formData: any) {
    formData.estado = 'A'
    this.book.postBook(formData).subscribe((res) => {
      this.toastr.success('Cita Agregada 💃');
      setTimeout(() => {
        this.reset();
      }, 2000);
    });
  }
}
