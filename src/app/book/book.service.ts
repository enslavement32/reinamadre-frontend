import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class BookService {
  data = 'http://localhost:8090/api/citas';
  
  constructor(private http: HttpClient) {}

  updateBook(id: any, input: any) {
    return this.http.put(`${this.data}/${id}`, input);
  }

  getBooks(): Observable<any> {
    return this.http.get(`${this.data}/all`);
  }

  getBook(id: any): Observable<any> {
    return this.http.get(`${this.data}/${id}`);
  }

  deleteBook(id: any): Observable<any> {
    return this.http.delete(`${this.data}/${id}`);
  }
  postBook(input: string): Observable<any> {
    return this.http.post<any>(`${this.data}/create`, input);
  }
   
  getTipoCita(): Observable<any> {
    return this.http.get(`${this.data}/tipocita/all`);
  }
}
